# Abstrakt
Cilem je aplikace zahrnujici UI, skrz ktere jde prehlednym zpusobem ovladat vytvareni nove a upravovani stavajici infrastruktury slouzici k prijimani a smerovani pozadavku na infrastrukturu v AWS. Zahrnuty budou cloud-nativni komponenty specificke pro AWS. 
![alt text](diagram.png)

# Pozadavky
* Komunikace s AWS APIs skrz SDK. (Cloudformation, Certificate Manager, Lambda, Dynamodb)
* Backend tvoreny serverless resenim, tvorenym skrz AWS Serverless Lambda
* Frontend komunikujici s timto serverless resenim
* Zobrazeni stavajici infrastruktury, ktera je jiz vytvorena
* Vytvareni a upravovani nove infrastruktury
* Sprava certifikatu uchovavanych v AWS Certificate Manageru - AWS UI je hrozne - nemozne vyhledavani
* Moznost vytvoreni infrastruktury a nasledne cekani, dokud se vysledna infrastruktura nevytvori - progress bar s pripadnym vyslednym kodem - Success/Error - Reason

# Technologie
* Nodejs - Lambda - backend
* Angular - Frontend
* CloudFormation - AWS infrastructure rollout

# Casovy plan
* 2-3h - planning
* 2h   - zakladni struktura projektu, inicializace AWS SDK
* 8h   - vytvoreni zakladni infrastruktury pro pristup k backendum + routing
	* CloudFront
	* API Gateway
	* S3
	* Certificate Manager
* 6h   - Authentikace + Autorizace
	* Autentikace vuci stavajicim uzivatelum z externi databaze
* 6h   - jadro - osetreni prace s CloudFormation a zpracovani jeho cyklu
	* Active polling na stav deploymentu infrastruktury
	* ~ progress
* 6h   - CRUD operace pro Cloudformation
	* Vytvareni, mazani, sprava CloudFormation Stacku
* 10h  - Front end - Making things pretty 🦄
	* Tabs
	* Progress bars
	* Lists
	* Details




